package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klasses;
    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klasses = new ArrayList<>();
    }
    public String introduce(){
        if (this.klasses == null) {
            return super.introduce() + " I am a teacher.";
        }
        else {
            String klassContain = "";
            for (Klass klass : klasses) {
                klassContain += klass.getNumber() + ", ";
            }
            klassContain = klassContain.substring(0,klassContain.length()-2);
            return super.introduce() + String.format(" I am a teacher. I teach Class %s.", klassContain);
        }
    }
    public void assignTo(Klass klass){
        this.klasses.add(klass);
    }
    public boolean belongsTo(Klass klass){
        if(this.klasses == null) {
            return false;
        }
        if(this.klasses.contains(klass)){
            return true;
        }
        return false;
    }
    public boolean isTeaching(Student student){
        return klasses.contains(student.getKlass());
    }
    public void say(Student student){
        System.out.println(String.format("I am %s, teacher of Class %d. I know %s become Leader.",
                this.getName(), student.getKlass().getNumber(), student.getName()));

    }
}
