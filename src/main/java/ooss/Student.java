package ooss;

public class Student extends Person {
    private Klass klass;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }


     public String introduce(){
        if(this.klass == null){
            return super.introduce() + " I am a student.";
        }
        else if(this.getKlass().getLeader() == null || this.getKlass().getLeader() != this){
            return super.introduce() + String.format(" I am a student. I am in class %d.", this.klass.getNumber());
        }
        else{
            return super.introduce() + String.format(" I am a student. I am the leader of class %d.", this.klass.getNumber());
        }
     }

    public void join(Klass klass){
        this.klass = klass;
    }
    public boolean isIn(Klass klass){
        if(this.klass == null) return false;
        if(this.klass.equals(klass)) return true;
        return false;
    }

    public Klass getKlass() {
        return klass;
    }
    public void say(Student student){
        System.out.println(String.format("I am %s, student of Class %d. I know %s become Leader.",
                this.getName(), this.klass.getNumber(), student.getName()));
    }
}
