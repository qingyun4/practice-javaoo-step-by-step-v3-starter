package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;
    private List<Person> persons;


    public Klass(int number) {
        this.number = number;
        persons = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (student.getKlass() == null || student.getKlass().getNumber() != this.number) {
            System.out.println("It is not one of us.");
        } else {
            this.leader = student;
            for (Person person : persons) {
                person.say(this.leader);
            }
        }
    }

    public boolean isLeader(Student student) {
        if (this.leader == null) {
            return false;
        }
        if (this.leader == student) {
            return true;
        }
        return false;
    }

    public Student getLeader() {
        return leader;
    }

    public void attach(Person person) {
         this.persons.add(person);
    }
}
